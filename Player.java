public class Player{

   private String name;
   private int number;
   private int points;
   private int assists;
   private int rebounds;
   private int steals;
   private int blocks;
   private int turnovers;
   private int fouls;
   private int onCourt = 0;
   
   public Player(String name, int number){
      setName(name);
      setNumber(number);
      setPoints(0);
      setAssists(0);
      setRebounds(0);
      setSteals(0);
      setBlocks(0);
      setTurnovers(0);
      setFouls(0);
   }
   public Player(){
      setPoints(0);
      setAssists(0);
      setRebounds(0);
      setSteals(0);
      setBlocks(0);
      setTurnovers(0);
      setFouls(0);
   }
   
   public void setName(String name){
      this.name = name;
   }
   public String getName(){
      return name;
   }
   
   public void setNumber(int num){
      number = num;
   }
   public int getNumber(){
      return number;
   }
   
   public void setPoints(int p){
      points = p;
   }
   public int getPoints(){
      return points;
   }
   
   public void setAssists(int a){
      assists = a;
   }
   public int getAssists(){
      return assists;
   }
   
   public void setRebounds(int r){
      rebounds = r;
   }
   public int getRebounds(){
      return rebounds;
   }
   
   public void setSteals(int s){
      steals = s;
   }
   public int getSteals(){
      return steals;
   }
   
   public void setBlocks(int b){
      blocks = b;
   }
   public int getBlocks(){
      return blocks;
   }
   
   public void setTurnovers(int t){
      turnovers = t;
   }
   public int getTurnovers(){
      return turnovers;
   }
   
   public void setFouls(int f){
      fouls = f;
   }
   public int getFouls(){
      return fouls;
   }
   
   public void setOnCourt(int onCourt){
      this.onCourt = onCourt;
   }
   public int getOnCourt(){
      return onCourt;
   }
   
   public String toString(){
      return name+" #"+number;
   }
}