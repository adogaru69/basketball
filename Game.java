/*
   Andrei Dogaru , STARTED: 2015/11/12 ,  LAST EDIT: 2015/11/15
   
      THINGS THAT NEED TO BE IMPROVED !!!
      -> print some new stats (free throws, 3 pointers, 2 pointers made/attempted ; offensive/defensive reb)
               (so need to add some new fields to the Player class)
      -> create some new text documents with other nba teams
      -> add some visuals (maybe see the stats changes live) 
*/

import java.util.*;
import java.io.*;

public class Game{

   static ArrayList<Player> teamA = new ArrayList<>();
   
   static ArrayList<Player> teamB = new ArrayList<>();
   
   // static variable that helps us see whether the rebound was offensive or defensive
   // and set which team has the possession of the ball
   // offDef = 1 -> offensive reb , offDef = 0 -> defensive reb
   static int offDef = -1;
   
   // static variable that helps us see if the player made his last freethrow attempt
   // and then set the possession of the ball
   // freeThrow = 1 -> last freethrow made , freeThrow = 0 -> miss
   static int freeThrow = 0;
   
   public static void main(String[] args)throws Exception{
      Scanner cin = new Scanner(System.in);
      
      // select your team and the starting five
      String myTeam = selectTeam(cin);
      System.out.println();
      startingFive(cin,teamA);
      System.out.println();
      
      // select your opponent's team and the starting five
      String opponent = selectOpponent(cin);
      System.out.println();
      startingFive(cin,teamB);
      System.out.println();
      
      Thread.sleep(700);
      
      // start game
      System.out.println();
      startGame(cin,myTeam,opponent);
   }
   
   // Select your team
   public static String selectTeam(Scanner cin)throws Exception{
      System.out.print("Enter the team you want to play with: ");
      String team = cin.next();
      Scanner fin = new Scanner(new File(team+".txt"));
      int count = 0;
      // read each name and player number from the file
      while(fin.hasNextLine()){
         String name = fin.next();
         while(!fin.hasNextInt()){
            name+=" "+fin.next();
         }
         int number = fin.nextInt();
         // create a player with those attributes and add it to the arrayList
         Player p = new Player(name,number);
         teamA.add(p);
      }
      // show the players you have in your arrayList teamA
      System.out.println("Your team has the following players: ");
      for(int i=0;i<teamA.size();i++){
         System.out.println(teamA.get(i));
      } 
      return team;
   }
   
   // Select your opponent's team
   public static String selectOpponent(Scanner cin)throws Exception{
      System.out.print("Enter the team you want to play against: ");
      String team = cin.next();
      Scanner fin = new Scanner(new File(team+".txt"));
      int count = 0;
      // read each name and player number from the file
      while(fin.hasNextLine()){
         String name = fin.next();
         while(!fin.hasNextInt()){
            name+=" "+fin.next();
         }
         int number = fin.nextInt();
         // create a player with those attributes and add it to the arrayList
         Player p = new Player(name,number);
         teamB.add(p);
      }
      // show the players you have in your arrayList teamB
      System.out.println("Your opponent has the following players: ");
      for(int i=0;i<teamB.size();i++){
         System.out.println(teamB.get(i));
      } 
      return team;
   }
   
   // Select the starting five
   public static void startingFive(Scanner cin,ArrayList<Player> team){
      System.out.println("Select the starting five using the player's numbers: ");
      // select the point guard
      System.out.print("PG: ");
      int pg = cin.nextInt();
      // the flag is used to see if there is any player with that number
      boolean flag = false;
      do{
         // if the player is found, we put him in the starting five
         for(int i=0;i<team.size();i++){
            if(pg == team.get(i).getNumber()){
               team.get(i).setOnCourt(1);
               flag = true;
               break;
            }
         }
         // if not, try again
         if(flag == false){
            System.out.println("You don't have a player with that number. Please try again.");
            System.out.print("PG: ");
            pg = cin.nextInt();
         }
      }while(flag == false);
      
      // select the shooting guard
      System.out.print("SG: ");
      int sg = cin.nextInt();
      // the flag is used to see if there is any player with that number
      flag = false;
      do{
         // verify if the desired player has already been selected
         while(verifySelection(sg,team) == false){
            System.out.println("You already selected that player. Please try again.");
            System.out.print("SG: ");
            sg = cin.nextInt();
         }
         // if the player is found, we put him in the starting five
         for(int i=0;i<team.size();i++){
            if(sg == team.get(i).getNumber()){
               team.get(i).setOnCourt(2);
               flag = true;
               break;
            }
         }
         // if not, try again
         if(flag == false){
            System.out.println("You don't have a player with that number. Please try again.");
            System.out.print("SG: ");
            sg = cin.nextInt();
         }
      }while(flag == false);
      
      // select the small forward
      System.out.print("SF: ");
      int sf = cin.nextInt();
      // the flag is used to see if there is any player with that number
      flag = false;
      do{
         // verify if the desired player has already been selected
         while(verifySelection(sf,team) == false){
            System.out.println("You already selected that player. Please try again.");
            System.out.print("SF: ");
            sf = cin.nextInt();
         }
         // if the player is found, we put him in the starting five
         for(int i=0;i<team.size();i++){
            if(sf == team.get(i).getNumber()){
               team.get(i).setOnCourt(3);
               flag = true;
               break;
            }
         }
         // if not, try again
         if(flag == false){
            System.out.println("You don't have a player with that number. Please try again.");
            System.out.print("SF: ");
            sf = cin.nextInt();
         }
      }while(flag == false);
      
      // select the power forward
      System.out.print("PF: ");
      int pf = cin.nextInt();
      // the flag is used to see if there is any player with that number
      flag = false;
      do{
         // verify if the desired player has already been selected
         while(verifySelection(pf,team) == false){
            System.out.println("You already selected that player. Please try again.");
            System.out.print("PF: ");
            pf = cin.nextInt();
         }
         // if the player is found, we put him in the starting five
         for(int i=0;i<team.size();i++){
            if(pf == team.get(i).getNumber()){
               team.get(i).setOnCourt(4);
               flag = true;
               break;
            }
         }
         // if not, try again
         if(flag == false){
            System.out.println("You don't have a player with that number. Please try again.");
            System.out.print("PF: ");
            pf = cin.nextInt();
         }
      }while(flag == false);
      
      // select the center
      System.out.print("C: ");
      int c = cin.nextInt();
      // the flag is used to see if there is any player with that number
      flag = false;
      do{
         // verify if the desired player has already been selected
         while(verifySelection(c,team) == false){
            System.out.println("You already selected that player. Please try again.");
            System.out.print("C: ");
            c = cin.nextInt();
         }
         // if the player is found, we put him in the starting five
         for(int i=0;i<team.size();i++){
            if(c == team.get(i).getNumber()){
               team.get(i).setOnCourt(5);
               flag = true;
               break;
            }
         }
         // if not, try again
         if(flag == false){
            System.out.println("You don't have a player with that number. Please try again.");
            System.out.print("C: ");
            c = cin.nextInt();
         }
      }while(flag == false);
      
      // print the starting five
      System.out.println("You have selected the following starting five: ");
      showPlayersOnCourt(team);
      System.out.println();
   }
   
   public static void showPlayersOnCourt(ArrayList<Player> team){
      int tempCount = 1;
      for(int i=0;i<5;i++){
         for(int j=0;j<team.size();j++){
            if(tempCount == team.get(j).getOnCourt()){
               System.out.println(team.get(j).getName()+" #"+team.get(j).getNumber());
               tempCount ++;
            }   
         }
      }
   }
   public static void showPlayersOnBench(ArrayList<Player> team){
      for(int i=0;i<team.size();i++){
         if(team.get(i).getOnCourt() == 0){
            System.out.println(team.get(i).getName()+" #"+team.get(i).getNumber());
         }
      }
   }
   
   // Verify if a player is on the court
   public static boolean verifySelection(int nr, ArrayList<Player> team){
      for(int i=0;i<team.size();i++){
         if(team.get(i).getNumber() == nr && team.get(i).getOnCourt() != 0)
            return false;
      }
      return true;
   }
   // Verify if a player is on the bench
   public static boolean verifyOnBench(int nr, ArrayList<Player> team){
      for(int i=0;i<team.size();i++){
         if(team.get(i).getNumber() == nr && team.get(i).getOnCourt() == 0)
            // he is on the bench
            return true;
      }
      return false;
   }
   
   public static void startGame(Scanner cin, String myTeam, String opponent){
      System.out.println("Start of Q1.");
      System.out.println("Select the team who won the tip-off."); 
      // "team" is used to select which team is in possession
      ArrayList<Player> team = new ArrayList<>();
      team = selectPossession(cin,myTeam,opponent);
      startQuarter(cin,myTeam,opponent,team);
      
      System.out.println();
      System.out.println("Start of Q2.");
      team = changePossession(team);  
      startQuarter(cin,myTeam,opponent,team);
      
      System.out.println();
      System.out.println("Start of Q3.");
      startQuarter(cin,myTeam,opponent,team);
      
      System.out.println();
      System.out.println("Start of Q4.");
      team = changePossession(team);  
      startQuarter(cin,myTeam,opponent,team);
      
      // end game
      System.out.println();
      System.out.println("                                                  GAME OVER !!!");
      System.out.println();
      // print all players + stats
      printStats(teamA);
      System.out.println();
      printStats(teamB);
      System.out.println();
      // final score ( + team stats )
      finalScore(myTeam,opponent);
   }
   
   // This is the menu where you can select the events that are happening
   public static void startQuarter(Scanner cin, String myTeam, String opponent,  ArrayList<Player> team){
      boolean flag = true;
      // "team" is used to select which team is in possession
      do{
         System.out.println("Select the events that are happening using the following instructions.");
         System.out.println("If a player shots the ball press 1.");
         System.out.println("If a player makes a turnover press 2.");
         System.out.println("If a player commits a foul press 3.");
         System.out.println("If there is a substitution press 4.");
         System.out.println("To end the quarter press 0.");
         int num = cin.nextInt();
         switch(num){
            case 1:
               // shot the ball ( you can also select if there was an assist,rebound or block made)
               shotBall(cin,team,myTeam,opponent);
               team = changePossession(team);
               // if it is an offensive rebound, we need to give the possession back
               // to the team who took the shoot 
               if(offDef == 1)
                  team = changePossession(team);
               // if(freeThrow == 1){
//                   // player made his last free throw attemp -> change possession of the ball
//                   team = changePossession(team);
//                }else{
//                   // free throw missed
//                   if(offDef == 0)
//                      // defensive rebound made -> change possession of the ball
//                      team = changePossession(team);
//                }
               break;
            case 2:
               // make turnover
               makeTurnover(cin,team);
               team = changePossession(team);
               break;
            case 3:
               // commit foul
               commitFoul(cin,team);
               if(freeThrow == 1){
                  // player made his last free throw attemp -> change possession of the ball
                  team = changePossession(team);
               }
               else{
                  // free throw missed
                  if(offDef == 0)
                     // defensive rebound made -> change possession of the ball
                     team = changePossession(team);
               }
               break;
            case 4:
               // substitution
               makeSubstitution(cin,team,myTeam,opponent);
               break;
            case 0:
               // end quarter
               flag = false;
               break;
            default:
               System.out.println("You have to enter 1,2,3,4 to play or 0 to end the quarter.");
               break;
         }
      }while(flag == true);
   }
   
   // Returns which team is in possession
   public static ArrayList<Player> selectPossession(Scanner cin, String myTeam, String opponent){
      String team = cin.next();
      boolean flag = false;
      while(flag == false){
         if(team.equalsIgnoreCase(myTeam)){
            return teamA;
         }
         else if(team.equalsIgnoreCase(opponent)){
            return teamB;
         }
         else{
            System.out.println("Error. Please enter a valid team name.");
            team = cin.next();
         }
      }
      return null;
   }
   
   // Changes the possession of the ball
   public static ArrayList<Player> changePossession(ArrayList<Player> team){
      if(team == teamA)
         return teamB;
      else
         return teamA;
   }
   
   public static void shotBall(Scanner cin, ArrayList<Player> team, String myTeam, String opponent){
      System.out.println("Select the player who took the shot (enter his number): ");
      showPlayersOnCourt(team);
      int num = cin.nextInt();
      // verify if the player is on the court
      while(verifySelection(num,team) == true){
         System.out.println("The player isn't on the court. Please choose a valid number.");
         num = cin.nextInt();
      }
      // shot action
      System.out.println("Press 1 if he made the shot or 0 if he missed.");
      int shot = cin.nextInt();
      while(shot != 0 && shot != 1){
         System.out.println("Error. Please read the instructions and try again.");
         shot = cin.nextInt();
      }
      if(shot == 0){
         // miss shot
         // verify if it was o foul on the play
         System.out.println("Was it a foul on the play ? Press 1 if yes, or 0 if no.");
         int foul = cin.nextInt(); 
         // verify if the user enters a valid number
         while(foul != 0 && foul != 1){
            System.out.println("Error. Please read the instructions and try again.");
            foul = cin.nextInt();
         }
         if(foul == 1){
            // foul on play
            System.out.println("Select the player who committed the foul (enter his number):");
            team = changePossession(team);
            showPlayersOnCourt(team);
            int num2 = cin.nextInt();
            // verify if the player is on the court
            while(verifySelection(num2,team) == true){
               System.out.println("The player isn't on the court. Please choose a valid number.");
               num2 = cin.nextInt();
            }
            // increment the player's fouls
            int i;
            for(i=0;i<team.size();i++){
               if(team.get(i).getNumber() == num2){
                  team.get(i).setFouls(team.get(i).getFouls()+1);
                  break;
               }
            }
            // verify if the player is legit to play (if he has less than 5 fouls) >>>> a player is ejected if he has 5 fouls
            if(team.get(i).getFouls() == 5){
               // force substitution
               System.out.println(team.get(i).getName()+" has 5 fouls. Make substitution !");
               System.out.println("Select the player you want to send in (enter his number):"); 
               showPlayersOnBench(team);
               // the flag is used to see if the player is legit to play
               boolean flag = true;
               int toCourt = cin.nextInt();
               for(;;){
                  // verify if the player is on the bench
                  while(verifyOnBench(toCourt,team) == false){
                     System.out.println("The player is on court or you don't have a player with that number. Please choose a valid number.");
                     toCourt = cin.nextInt();
                  }
                  // verify if the player is legit to play (has less than 5 fouls)
                  for(int j=0;j<team.size();j++){
                     if(team.get(j).getNumber() == toCourt && team.get(j).getFouls() == 5){
                        flag = false;
                        System.out.println("This player isn't legit to play (has 5 fouls). Please select another player.");
                        toCourt = cin.nextInt();
                        break;
                     }
                     else{
                        flag = true;
                     }
                  }
                  if(verifyOnBench(toCourt,team) == true && flag == true)
                     break;
               }
               for(int j=0;j<team.size();j++){
                  if(team.get(j).getNumber() == toCourt){
                     // the player from the bench is send to court in the position of the first player
                     team.get(j).setOnCourt(team.get(i).getOnCourt());
                     // send the first player to the bench
                     team.get(i).setOnCourt(0);
                  }
               }
            }
            team = changePossession(team);
            System.out.println("Press 3 if he was fouled at the 3 point line, otherwise press 2.");
            shot = cin.nextInt();  
            // verify if the user enters a valid number 
            while(shot != 2 && shot != 3){
               System.out.println("Error. Please read the instructions and try again.");
               shot = cin.nextInt();
            }
            // add the points he gets from the free throws to his total points
            int points = 0;
            System.out.println("Did he made the first free throw ? Enter 1 if yes, or 0 if no.");
            int made = cin.nextInt();
            // verify if the user enters a valid number 
            while(made != 0 && made != 1){
               System.out.println("Error. Please read the instructions and try again.");
               made = cin.nextInt();
            }
            if(made == 1){
               points ++;
            }
            System.out.println("Did he made the second free throw ? Enter 1 if yes, or 0 if no.");
            made = cin.nextInt();
            // verify if the user enters a valid number 
            while(made != 0 && made != 1){
               System.out.println("Error. Please read the instructions and try again.");
               made = cin.nextInt();
            }
            if(made == 1){
               points ++;
            }
            if(shot == 3){
               // he also needs to shoot a 3rd free throw
               System.out.println("Did he made the third free throw ? Enter 1 if yes, or 0 if no.");
               made = cin.nextInt();
               // verify if the user enters a valid number 
               while(made != 0 && made != 1){
                  System.out.println("Error. Please read the instructions and try again.");
                  made = cin.nextInt();
               }
               if(made == 1){
                  points ++;
               }
            }
            if(made == 1){
               // if he makes the last free throw -> change possession
               freeThrow = 1;
               offDef = 0;
            }
            else{
               // else -> see if it was an offensive or defensive rebound
               freeThrow = 0;
               // verify if it's an offensive or defensive rebound
               System.out.println("Enter 1 if it was an offensive rebound, or 0 if it was a defensive rebound.");
               offDef = cin.nextInt();
               // verify if the user enters a valid number
               while(offDef != 0 && offDef != 1){
                  System.out.println("Error. Please read the instructions and try again.");
                  offDef = cin.nextInt();
               }
               if(offDef == 0){
                  // if it's defensive, we need to change the team in possession 
                  team = changePossession(team);
               }
               // select the player who made the rebound
               System.out.println("Select the player who made the rebound (enter his number):");
               showPlayersOnCourt(team);
               int reb = cin.nextInt();
               // verify if the player is on the court
               while(verifySelection(reb,team) == true){
                  System.out.println("The player isn't on the court. Please choose a valid number.");
                  reb = cin.nextInt();
               }
               // increment the player's rebounds
               for(int j=0;j<team.size();j++){
                  if(team.get(j).getNumber() == reb){
                     team.get(j).setRebounds(team.get(j).getRebounds()+1);
                  }
               }
            }
            for(int j=0;j<team.size();j++){
               if(team.get(j).getNumber() == num){
                  team.get(j).setPoints(team.get(j).getPoints()+points);
               }
            }
         }
         else{
            // no foul
            // verify if it was a block or not
            System.out.println("Was it a block ? Press 1 if yes, or 0 if no.");
            shot = cin.nextInt();
            // verify if the user enters a valid number
            while(shot != 0 && shot != 1){
               System.out.println("Error. Please read the instructions and try again.");
               shot = cin.nextInt();
            }
            if(shot == 0){
               // just a miss shot
            }
            else{
               // block shot
               // change the team so we can select the player who blocked the shot
               team = changePossession(team);
               System.out.println("Select the player who blocked the shot (enter his number):");
               showPlayersOnCourt(team);
               int block = cin.nextInt();
               // verify if the player is on the court
               while(verifySelection(block,team) == true){
                  System.out.println("The player isn't on the court. Please choose a valid number.");
                  block = cin.nextInt();
               }
               // increment the player's blocks
               for(int i=0;i<team.size();i++){
                  if(team.get(i).getNumber() == block){
                     team.get(i).setBlocks(team.get(i).getBlocks()+1);
                  }
               }
               // change the possession again so we can continue the game
               team = changePossession(team);
            }
            // verify if it was a rebound
            System.out.println("Was it a rebound ? Press 1 if yes, or 0 if no.");
            shot = cin.nextInt();
            // verify if the user enters a valid number
            while(shot != 0 && shot != 1){
               System.out.println("Error. Please read the instructions and try again.");
               shot = cin.nextInt();
            }
            if(shot == 1){
               // rebound made
               // verify if it's an offensive or defensive rebound
               System.out.println("Enter 1 if it was an offensive rebound, or 0 if it was a defensive rebound.");
               offDef = cin.nextInt();
               // verify if the user enters a valid number
               while(offDef != 0 && offDef != 1){
                  System.out.println("Error. Please read the instructions and try again.");
                  offDef = cin.nextInt();
               }
               if(offDef == 0){
                  // if it's defensive, we need to change the team in possession 
                  team = changePossession(team);
               }
               // select the player who made the rebound
               System.out.println("Select the player who made the rebound (enter his number):");
               showPlayersOnCourt(team);
               int reb = cin.nextInt();
               // verify if the player is on the court
               while(verifySelection(reb,team) == true){
                  System.out.println("The player isn't on the court. Please choose a valid number.");
                  reb = cin.nextInt();
               }
               // increment the player's rebounds
               for(int i=0;i<team.size();i++){
                  if(team.get(i).getNumber() == reb){
                     team.get(i).setRebounds(team.get(i).getRebounds()+1);
                  }
               }
            }
            else{
               // miss shoot and also no rebound made
               // set the offDef variable back to -1 so it won't affect the possession of the ball
               offDef = -1;
            }
         }
      }
      else{
         // made shot
         // first set the offDef variable back to -1 so it won't affect the possession of the ball
         offDef = -1;
         freeThrow = 0;
         System.out.println("Enter 3 if it was a 3 pointer or 2 if it was a 2 pointer. ");
         shot = cin.nextInt();
         // verify if the user enters a valid number
         while(shot != 2 && shot != 3){
            System.out.println("Error. Please read the instructions and try again.");
            shot = cin.nextInt();
         }
         // increment the player's points by 2 or 3
         for(int i=0;i<team.size();i++){
            if(team.get(i).getNumber() == num){
               team.get(i).setPoints(team.get(i).getPoints()+shot);
            }
         }
         // verify if it was also an assist
         System.out.println("Was it an assist in this play? Enter 1 if yes, or 0 if no.");
         shot = cin.nextInt();
         // verify if the user enters a valid number
         while(shot != 0 && shot != 1){
            System.out.println("Error. Please read the instructions and try again.");
            shot = cin.nextInt();
         }
         if(shot == 1){
            // assist made
            System.out.println("Select the player who made the assist (enter his number):");
            showPlayersOnCourt(team);
            int ass = cin.nextInt();
            // verify if the player is on the court
            while(verifySelection(ass,team) == true || ass == num){
               System.out.println("The player isn't on the court or you have selected the same player who took the shot."+
                                  "Please choose a valid number.");
               ass = cin.nextInt();
            }
            // increment the player's assists
            for(int i=0;i<team.size();i++){
               if(team.get(i).getNumber() == ass){
                  team.get(i).setAssists(team.get(i).getAssists()+1);
               }
            }
         }
         // verify if it was also a foul on the play
         System.out.println("Was it a foul on the play ? Enter 1 if yes, or 0 if no.");
         int foul = cin.nextInt();
         // verify if the user enters a valid number
         while(foul != 0 && foul != 1){
            System.out.println("Error. Please read the instructions and try again.");
            foul = cin.nextInt();
         }
         if(foul == 1){
            // foul made
            System.out.println("Select the player who made the foul (enter his number):");
            team = changePossession(team);
            showPlayersOnCourt(team);
            foul = cin.nextInt();
            // verify if the player is on the court
            while(verifySelection(foul,team) == true){
               System.out.println("The player isn't on the court. Please choose a valid number.");
               foul = cin.nextInt();
            }
            // increment the player's fouls
            int k;
            for(k=0;k<team.size();k++){
               if(team.get(k).getNumber() == foul){
                  team.get(k).setFouls(team.get(k).getFouls()+1);
                  break;
               }
            }
            // verify if the player is legit to play (if he has less than 5 fouls) >>>> a player is ejected if he has 5 fouls
            if(team.get(k).getFouls() == 5){
               // make substitution
               System.out.println(team.get(k).getName()+" has 5 fouls. Make substitution !");
               System.out.println("Select the player you want to send in (enter his number):"); 
               showPlayersOnBench(team);
               // the flag is used to see if the player is legit to play
               boolean flag = true;
               int toCourt = cin.nextInt();
               for(;;){
                  // verify if the player is on the bench
                  while(verifyOnBench(toCourt,team) == false){
                     System.out.println("The player is on court or you don't have a player with that number. Please choose a valid number.");
                     toCourt = cin.nextInt();
                  }
                  // verify if the player is legit to play (has less than 5 fouls)
                  for(int j=0;j<team.size();j++){
                     if(team.get(j).getNumber() == toCourt && team.get(j).getFouls() == 5){
                        flag = false;
                        System.out.println("This player isn't legit to play (has 5 fouls). Please select another player.");
                        toCourt = cin.nextInt();
                        break;
                     }
                     else{
                        flag = true;
                     }
                  }
                  if(verifyOnBench(toCourt,team) == true && flag == true)
                     break;
               }
               for(int j=0;j<team.size();j++){
                  if(team.get(j).getNumber() == toCourt){
                     // the player from the bench is send to court in the position of the first player
                     team.get(j).setOnCourt(team.get(k).getOnCourt());
                     // send the first player to the bench
                     team.get(k).setOnCourt(0);
                  }
               }
            }
            // change again the possession so we can handle the player who is shooting the additional free throw
            team = changePossession(team);
            System.out.println("Did he make the additional free throw ? Enter 1 if yes, or 0 if no.");
            foul = cin.nextInt();
            // verify if the user enters a valid number
            while(foul != 0 && foul != 1){
               System.out.println("Error. Please read the instructions and try again.");
               foul = cin.nextInt();
            }
            if(foul == 1){
               // additional free throw made
               // increment the player's points
               for(int i=0;i<team.size();i++){
                  if(team.get(i).getNumber() == num){
                     team.get(i).setPoints(team.get(i).getPoints()+1);
                  }
               }
               freeThrow = 1;
            }
            else{
               // miss
               freeThrow = 0;
               // see if it was an offensive or defensive rebound
               // verify if it's an offensive or defensive rebound
               System.out.println("Enter 1 if it was an offensive rebound, or 0 if it was a defensive rebound.");
               offDef = cin.nextInt();
               // verify if the user enters a valid number
               while(offDef != 0 && offDef != 1){
                  System.out.println("Error. Please read the instructions and try again.");
                  offDef = cin.nextInt();
               }
               if(offDef == 0){
                  // if it's defensive, we need to change the team in possession 
                  team = changePossession(team);
               }
               // select the player who made the rebound
               System.out.println("Select the player who made the rebound (enter his number):");
               showPlayersOnCourt(team);
               int reb = cin.nextInt();
               // verify if the player is on the court
               while(verifySelection(reb,team) == true){
                  System.out.println("The player isn't on the court. Please choose a valid number.");
                  reb = cin.nextInt();
               }
               // increment the player's rebounds
               for(int j=0;j<team.size();j++){
                  if(team.get(j).getNumber() == reb){
                     team.get(j).setRebounds(team.get(j).getRebounds()+1);
                  }
               }
            }
         } 
      }  
   }
   
   public static void makeTurnover(Scanner cin, ArrayList<Player> team){
      System.out.println("Select the player who made the turnover (enter his number): ");
      showPlayersOnCourt(team);
      int num = cin.nextInt();
      // verify if the player is on the court
      while(verifySelection(num,team) == true){
         System.out.println("The player isn't on the court. Please choose a valid number.");
         num = cin.nextInt();
      }
      // increment the player's turnovers
      for(int i=0;i<team.size();i++){
         if(team.get(i).getNumber() == num){
            team.get(i).setTurnovers(team.get(i).getTurnovers()+1);
         }
      }
      // after the turnover, we need to change possession of the ball
      team = changePossession(team);
      // see if there was a steal
      System.out.println("Was it a steal on the play ? Enter 1 if yes, or 0 if no.");
      int st = cin.nextInt();  
      // verify if the user enters a valid number 
      while(st != 0 && st != 1){
         System.out.println("Error. Please read the instructions and try again.");
         st = cin.nextInt();
      } 
      if(st == 1){
         // steal made
         System.out.println("Select the player that made the steal (enter his number):");
         showPlayersOnCourt(team);
         st = cin.nextInt();
         // verify if the player is on the court
         while(verifySelection(st,team) == true){
            System.out.println("The player isn't on the court. Please choose a valid number.");
            st = cin.nextInt();
         }
         // increment the player's steals
         for(int i=0;i<team.size();i++){
            if(team.get(i).getNumber() == num){
               team.get(i).setSteals(team.get(i).getSteals()+1);
            }   
         }
      }
   }
   
   public static void commitFoul(Scanner cin, ArrayList<Player> team){
      System.out.println("Select the player who committed the foul (enter his number):");
      team = changePossession(team);
      showPlayersOnCourt(team);
      int num = cin.nextInt();
      // verify if the player is on the court
      while(verifySelection(num,team) == true){
         System.out.println("The player isn't on the court. Please choose a valid number.");
         num = cin.nextInt();
      }
      // increment the player's fouls
      int i;
      for(i=0;i<team.size();i++){
         if(team.get(i).getNumber() == num){
            team.get(i).setFouls(team.get(i).getFouls()+1);
            break;
         }
      }
      // verify if the player is legit to play (if he has less than 5 fouls) >>>> a player is ejected if he has 5 fouls
      if(team.get(i).getFouls() == 5){
         // force substitution
         System.out.println(team.get(i).getName()+" has 5 fouls. Make substitution !");
         System.out.println("Select the player you want to send in (enter his number):"); 
         showPlayersOnBench(team);
         // the flag is used to see if the player is legit to play
         boolean flag = true;
         int toCourt = cin.nextInt();
         for(;;){
            // verify if the player is on the bench
            while(verifyOnBench(toCourt,team) == false){
               System.out.println("The player is on court or you don't have a player with that number. Please choose a valid number.");
               toCourt = cin.nextInt();
            }
            // verify if the player is legit to play (has less than 5 fouls)
            for(int j=0;j<team.size();j++){
               if(team.get(j).getNumber() == toCourt && team.get(j).getFouls() == 5){
                  flag = false;
                  System.out.println("This player isn't legit to play (has 5 fouls). Please select another player.");
                  toCourt = cin.nextInt();
                  break;
               }
               else{
                  flag = true;
               }
            }
            if(verifyOnBench(toCourt,team) == true && flag == true)
               break;
         }
         for(int j=0;j<team.size();j++){
            if(team.get(j).getNumber() == toCourt){
               // the player from the bench is send to court in the position of the first player
               team.get(j).setOnCourt(team.get(i).getOnCourt());
               // send the first player to the bench
               team.get(i).setOnCourt(0);
            }
         }
      }
      // verify if the foul was committed while another player was shooting
      System.out.println("Was the fouled player in a shooting motion ? Enter 1 if yes, or 0 if no.");
      int shot = cin.nextInt();  
      // verify if the user enters a valid number 
      while(shot != 0 && shot != 1){
         System.out.println("Error. Please read the instructions and try again.");
         shot = cin.nextInt();
      }
      if(shot == 1){
         // shooting free throws
         System.out.println("Press 3 if he was fouled at the 3 point line, otherwise press 2.");
         shot = cin.nextInt();  
         // verify if the user enters a valid number 
         while(shot != 2 && shot != 3){
            System.out.println("Error. Please read the instructions and try again.");
            shot = cin.nextInt();
         }
         // see which player was fouled
         System.out.println("Select the player who was fouled (enter his number):");
         team = changePossession(team);
         showPlayersOnCourt(team);
         num = cin.nextInt();
         // verify if the player is on the court
         while(verifySelection(num,team) == true){
            System.out.println("The player isn't on the court. Please choose a valid number.");
            num = cin.nextInt();
         }
         // add the points he gets from the free throws to his total points
         int points = 0;
         System.out.println("Did he made the first free throw ? Enter 1 if yes, or 0 if no.");
         int made = cin.nextInt();
         // verify if the user enters a valid number 
         while(made != 0 && made != 1){
            System.out.println("Error. Please read the instructions and try again.");
            made = cin.nextInt();
         }
         if(made == 1){
            points ++;
         }
         System.out.println("Did he made the second free throw ? Enter 1 if yes, or 0 if no.");
         made = cin.nextInt();
         // verify if the user enters a valid number 
         while(made != 0 && made != 1){
            System.out.println("Error. Please read the instructions and try again.");
            made = cin.nextInt();
         }
         if(made == 1){
            points ++;
         }
         if(shot == 3){
            // he also needs to shoot a 3rd free throw
            System.out.println("Did he made the third free throw ? Enter 1 if yes, or 0 if no.");
            made = cin.nextInt();
            // verify if the user enters a valid number 
            while(made != 0 && made != 1){
               System.out.println("Error. Please read the instructions and try again.");
               made = cin.nextInt();
            }
            if(made == 1){
               points ++;
            }
         }
         if(made == 1){
            // if he makes the last free throw -> change possession
            freeThrow = 1;
         }
         else{
            // else -> see if it was an offensive or defensive rebound
            freeThrow = 0;
            // verify if it's an offensive or defensive rebound
            System.out.println("Enter 1 if it was an offensive rebound, or 0 if it was a defensive rebound.");
            offDef = cin.nextInt();
            // verify if the user enters a valid number
            while(offDef != 0 && offDef != 1){
               System.out.println("Error. Please read the instructions and try again.");
               offDef = cin.nextInt();
            }
            if(offDef == 0){
               // if it's defensive, we need to change the team in possession 
               team = changePossession(team);
            }
            // select the player who made the rebound
            System.out.println("Select the player who made the rebound (enter his number):");
            showPlayersOnCourt(team);
            int reb = cin.nextInt();
            // verify if the player is on the court
            while(verifySelection(reb,team) == true){
               System.out.println("The player isn't on the court. Please choose a valid number.");
               reb = cin.nextInt();
            }
            // increment the player's rebounds
            for(int j=0;j<team.size();j++){
               if(team.get(j).getNumber() == reb){
                  team.get(j).setRebounds(team.get(j).getRebounds()+1);
               }
            }
         }
         for(int j=0;j<team.size();j++){
            if(team.get(j).getNumber() == num){
               team.get(j).setPoints(team.get(j).getPoints()+points);
            }
         }
      }
      else{
         // no free throws
         // set the variable back to -1 so it won't affect the possession of the ball
         offDef = -1;
         freeThrow = 0;
      }
   }
   
   public static void makeSubstitution(Scanner cin, ArrayList<Player> team, String myTeam, String opponent){
      System.out.print("Select the team who made the substitution :");
      team = selectPossession(cin,myTeam,opponent);
      System.out.println("Select the player you want to send to bench (enter his number):");
      showPlayersOnCourt(team);
      int toBench = cin.nextInt();
      // verify if the player is on the court
      while(verifySelection(toBench,team) == true){
         System.out.println("The player isn't on the court. Please choose a valid number.");
         toBench = cin.nextInt();
      }
      System.out.println("Select the player you want to send in (enter his number):"); 
      showPlayersOnBench(team);
      // the flag is used to see if the player is legit to play
      boolean flag = true;
      int toCourt = cin.nextInt();
      for(;;){
         // verify if the player is on the bench
         while(verifyOnBench(toCourt,team) == false){
            System.out.println("The player is on court or you don't have a player with that number. Please choose a valid number.");
            toCourt = cin.nextInt();
         }
         // verify if the player is legit to play (has less than 5 fouls)
         for(int i=0;i<team.size();i++){
            if(team.get(i).getNumber() == toCourt && team.get(i).getFouls() == 5){
               flag = false;
               System.out.println("This player isn't legit to play (has 5 fouls). Please select another player.");
               toCourt = cin.nextInt();
               break;
            }
            else{
               flag = true;
            }
         }
         if(verifyOnBench(toCourt,team) == true && flag == true)
            break;
      }
      // first we need to find in the array the player that we need to send to bench
      int i;
      for(i=0;i<team.size();i++){
         if(team.get(i).getNumber() == toBench){
            break;
         }
      }
      for(int j=0;j<team.size();j++){
         if(team.get(j).getNumber() == toCourt){
            // after that, we send to court (in the position of the first player) the player from the bench 
            team.get(j).setOnCourt(team.get(i).getOnCourt());
            // and we put the first player on the bench
            team.get(i).setOnCourt(0);
         }
      }
   }
   
   // Print all players + stats from a team
   public static void printStats(ArrayList<Player> team){
      System.out.println("                Name       Points   Assists   Rebounds   Steals   Blocks   Turnovers   Fouls");
      for(int i=0;i<team.size();i++){
         System.out.printf("%25s %5d %8d %9d %10d %8d %9d %9d\n",team.get(i).getName(),team.get(i).getPoints(),team.get(i).getAssists(),
                           team.get(i).getRebounds(),team.get(i).getSteals(),team.get(i).getBlocks(),team.get(i).getTurnovers(),team.get(i).getFouls());
      }
   }
   
   // Final score
   public static void finalScore(String myTeam, String opponent){
      System.out.println("                                                  The final score is :");
      int myScore = 0;
      for(int i=0;i<teamA.size();i++){
         myScore += teamA.get(i).getPoints();
      }
      int oppScore = 0;
      for(int i=0;i<teamB.size();i++){
         oppScore += teamB.get(i).getPoints();
      }
      System.out.println("                                                    "+myTeam+" "+myScore+" - "+oppScore+" "+opponent);
   }
}